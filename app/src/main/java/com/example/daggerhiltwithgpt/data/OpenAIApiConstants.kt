package com.example.daggerhiltwithgpt.data

class OpenAIApiConstants {

    companion object {
        const val BASE_URL = "https://api.openai.com"
        const val SERVER_KEY = "Add your key here"
        const val CONTENT_TYPE = "application/json"
    }
}