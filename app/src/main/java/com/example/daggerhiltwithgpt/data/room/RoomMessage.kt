package com.example.daggerhiltwithgpt.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.daggerhiltwithgpt.data.ChatMessage
import java.util.*

@Entity(tableName = "Messages2")
data class RoomMessage(
    var senderIsUser: Boolean = true,
    var time: Long = Date().time,
    var messageContent: String = "Default",
    var wasError: Boolean = false,

    @PrimaryKey(autoGenerate = true)
    var messageID: Int = 0,

    )