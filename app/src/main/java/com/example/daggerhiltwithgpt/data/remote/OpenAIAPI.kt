package com.example.daggerhiltwithgpt.data.remote


import com.example.daggerhiltwithgpt.data.GPTMessage
import com.example.daggerhiltwithgpt.data.GptResponseModel
import com.example.daggerhiltwithgpt.data.OpenAIApiConstants
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface OpenAIAPI {

    @Headers("Content-Type: ${OpenAIApiConstants.CONTENT_TYPE}","Authorization: Bearer ${OpenAIApiConstants.SERVER_KEY}")
    @POST("/v1/chat/completions")
    suspend fun postGPTMessage(
        @Body message: GPTMessage
    ): Response<GptResponseModel>
}