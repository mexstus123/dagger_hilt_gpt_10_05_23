package com.example.daggerhiltwithgpt.data.repository

import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import com.example.daggerhiltwithgpt.data.ChatMessage
import com.example.daggerhiltwithgpt.data.GPTMessage
import com.example.daggerhiltwithgpt.data.remote.OpenAIAPI
import com.example.daggerhiltwithgpt.data.room.RoomMessage
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GptRepositoryImpl @Inject constructor(
    private val api: OpenAIAPI,
    private val appContext: Application
): GptRepository {


    override suspend fun sendGPTMessage(messageList: List<ChatMessage>): RoomMessage? {
        val response = try {
            api.postGPTMessage(GPTMessage(messages = messageList))

        }catch (e: IOException){
            Log.e(TAG, "IOException, no internet connection? $e")
            return null
        }catch (e: HttpException){
            Log.e(TAG, "HttpException, unexpected response")
            return null
        }
        if (response.isSuccessful && response.body() != null){
            return RoomMessage(
                messageContent = response.body()!!.choices[0].message.content,
                senderIsUser = false
            )
        }else{
            Log.e(TAG, "Response not successful ${response.code()}")
            return null
        }

    }
}