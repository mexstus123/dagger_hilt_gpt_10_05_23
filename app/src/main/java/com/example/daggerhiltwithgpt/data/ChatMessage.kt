package com.example.daggerhiltwithgpt.data

data class ChatMessage(
    val role: String = "user",
    val content: String,
)