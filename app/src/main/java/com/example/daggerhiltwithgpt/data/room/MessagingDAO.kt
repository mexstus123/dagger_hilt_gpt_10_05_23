package com.example.daggerhiltwithgpt.data.room

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Data Access Object for the UserWord data class.
 * Contains all REST calls to the database.
 * [putMessage] Inserts a single Message object into the database.
 * [getAllMessages] gets all messages from the db.
 */
@Dao
interface MessagingDAO {
    @Insert
    suspend fun putMessage(message: RoomMessage)

    @Update
    suspend fun updateMessage(message: RoomMessage)

    @Query("DELETE FROM Messages2 WHERE time = :messageTime")
    suspend fun deleteMessage(messageTime: Long)

    @Query("SELECT * FROM Messages2 ORDER BY time DESC")
    fun getAllMessages(): LiveData<List<RoomMessage>>


}