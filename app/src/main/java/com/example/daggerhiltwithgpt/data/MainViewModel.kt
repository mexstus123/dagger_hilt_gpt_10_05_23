package com.example.daggerhiltwithgpt.data

import android.speech.tts.TextToSpeech
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.daggerhiltwithgpt.data.room.RoomMessage
import com.example.daggerhiltwithgpt.data.repository.GptRepository
import com.example.daggerhiltwithgpt.data.repository.MessagingRepo
import com.example.daggerhiltwithgpt.data.repository.MessagingRepository
import com.example.daggerhiltwithgpt.data.room.MessagingDAO
import com.example.daggerhiltwithgpt.data.room.MessagingRoomDataBase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: GptRepository,
    private val ioDispatcher: CoroutineDispatcher,
   private val messagingRepository: MessagingDAO
): ViewModel() {


    //setting up coroutineScope with dispatcher from constructor for testing
    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)

    lateinit var tts: TextToSpeech

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _replyReceived = MutableStateFlow(false)
    val replyReceived = _replyReceived.asStateFlow()

    //setting up the message list to be observed
    private var _messageList: LiveData<List<RoomMessage>> = messagingRepository.getAllMessages()
    val messageList: LiveData<List<RoomMessage>> get() = _messageList

    private var _gptMessageList: MutableList<ChatMessage> = mutableListOf()



    /**
     * Initializes the _messageList LiveData object by fetching all data from the messagingRepository.
     */
    fun initializeMessages(){
        coroutineScope.launch {
            _isLoading.value = true

            _messageList.value?.forEach {
                _gptMessageList.add(ChatMessage(content = it.messageContent))
            }
            _isLoading.value = false
        }

    }


    fun sendMessage(message: ChatMessage){
        coroutineScope.launch {
            _isLoading.value = true

            val roomMessage = RoomMessage(
                true,
                Date().time,
                message.content,
            )

            messagingRepository.putMessage(
                roomMessage
            )
            _gptMessageList.add(message)

            val response = repository.sendGPTMessage(_gptMessageList)
            if (response != null){
                messagingRepository.putMessage(response)
                _replyReceived.value = true
            }else{
                messagingRepository.deleteMessage(roomMessage.time)
                _gptMessageList.remove(message)
                roomMessage.wasError = true
                messagingRepository.putMessage(roomMessage)
            }

            _isLoading.value = false
        }
    }


    fun resetReplyReceived(){
        _replyReceived.value = false
    }
}

