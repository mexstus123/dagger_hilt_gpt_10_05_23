package com.example.daggerhiltwithgpt.data.repository

import androidx.lifecycle.LiveData
import com.example.daggerhiltwithgpt.data.ChatMessage
import com.example.daggerhiltwithgpt.data.room.RoomMessage

interface MessagingRepo {
    suspend fun updateMessage(message: RoomMessage)
    suspend fun putMessage(message: RoomMessage )
    fun getAllMessages(): LiveData<List<RoomMessage>>
}