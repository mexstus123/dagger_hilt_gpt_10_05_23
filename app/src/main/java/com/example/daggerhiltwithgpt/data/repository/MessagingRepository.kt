package com.example.daggerhiltwithgpt.data.repository

import com.example.daggerhiltwithgpt.data.room.MessagingDAO
import com.example.daggerhiltwithgpt.data.room.RoomMessage
import javax.inject.Inject

class MessagingRepository @Inject constructor(
    private val dao: MessagingDAO,
):MessagingRepo {


    override suspend fun updateMessage(message: RoomMessage) = dao.updateMessage(message)
    override suspend fun putMessage(message: RoomMessage) = dao.putMessage(message)
    override fun getAllMessages() = dao.getAllMessages()

}