package com.example.daggerhiltwithgpt.data.repository

import com.example.daggerhiltwithgpt.data.ChatMessage
import com.example.daggerhiltwithgpt.data.room.RoomMessage

interface GptRepository {
    suspend fun sendGPTMessage(messageList: List<ChatMessage>): RoomMessage?
}