package com.example.daggerhiltwithgpt.data

data class GPTMessage(
    val model: String = "gpt-3.5-turbo",
    val messages: List<ChatMessage>
)