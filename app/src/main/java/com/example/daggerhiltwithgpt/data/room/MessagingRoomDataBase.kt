package com.example.daggerhiltwithgpt.data.room

import android.app.Application
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import javax.inject.Singleton


@Database(entities = [RoomMessage::class], version = 2)
abstract class MessagingRoomDataBase : RoomDatabase() {


    /**
     * Returns a MessagingDAO object that can be used to interact with the database.
     * @return The MessagingDAO object associated with the database.
     */
    abstract fun messagingDao(): MessagingDAO

//    companion object {
//        private var instance: MessagingRoomDataBase? = null
//
//        /**
//         * Returns an instance of MessagingRoomDataBase.
//         *
//         * @param context The application context.
//         * @return An instance of MessagingRoomDataBase.
//         */
//        @Synchronized
//        fun getDatabase(app: Application = Application()): MessagingRoomDataBase? {
//            if (instance == null) {
//                instance =
//                    Room.databaseBuilder<MessagingRoomDataBase>(
//                        app,
//                        MessagingRoomDataBase::class.java,
//                        "GPTChat_DataBase"
//                    )
//                        .addCallback(roomDatabaseCallback(app))
//                        .build()
//            }
//            return instance
//        }
//
//
//        private fun roomDatabaseCallback(context: Context): Callback {
//            return object : Callback() {
//                //TODO add dummy data here so its full when they open it first time
//            }
//        }
//    }
}