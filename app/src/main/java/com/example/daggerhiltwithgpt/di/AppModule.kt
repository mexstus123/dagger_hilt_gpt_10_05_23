package com.example.daggerhiltwithgpt.di


import android.app.Application
import android.speech.tts.TextToSpeech
import com.example.daggerhiltwithgpt.data.GPTMessage
import com.example.daggerhiltwithgpt.data.OpenAIApiConstants
import com.example.daggerhiltwithgpt.data.remote.OpenAIAPI
import com.example.daggerhiltwithgpt.data.repository.GptRepositoryImpl
import com.example.daggerhiltwithgpt.data.room.MessagingDAO
import com.example.daggerhiltwithgpt.data.repository.GptRepository
import com.example.daggerhiltwithgpt.data.repository.MessagingRepo
import com.example.daggerhiltwithgpt.data.repository.MessagingRepository
import com.example.daggerhiltwithgpt.data.room.MessagingRoomDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideMyApi(timeOut: OkHttpClient): OpenAIAPI {
        return Retrofit.Builder()
            .baseUrl(OpenAIApiConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(timeOut)
            .build()
            .create(OpenAIAPI::class.java)
    }

    @Provides
    @Singleton
    fun okHttpClient() = OkHttpClient.Builder()
        .connectTimeout(5, TimeUnit.MINUTES)
        .readTimeout(2, TimeUnit.MINUTES)
        .writeTimeout(3, TimeUnit.MINUTES)
        .build()



    @Provides
    @Singleton
    fun provideIOThread(): CoroutineDispatcher {
        return Dispatchers.IO
    }
//
    @Provides
    @Singleton
    fun provideGptRepository(api: OpenAIAPI, app: Application): GptRepository {
        return GptRepositoryImpl(api, app)
    }




//    @Provides
//    @Singleton
//    @Named("hello1")
//    fun provideString1() = "Hello 1"
//
//    @Provides
//    @Singleton
//    @Named("hello2")
//    fun provideString2() = "Hello 2"
}