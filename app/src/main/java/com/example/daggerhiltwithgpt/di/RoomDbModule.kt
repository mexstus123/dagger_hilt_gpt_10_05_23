package com.example.daggerhiltwithgpt.di


import android.app.Application
import androidx.room.AutoMigration
import androidx.room.Room
import androidx.room.migration.Migration
import com.example.daggerhiltwithgpt.data.remote.OpenAIAPI
import com.example.daggerhiltwithgpt.data.repository.GptRepositoryImpl
import com.example.daggerhiltwithgpt.data.room.MessagingDAO
import com.example.daggerhiltwithgpt.data.repository.GptRepository
import com.example.daggerhiltwithgpt.data.repository.MessagingRepo
import com.example.daggerhiltwithgpt.data.repository.MessagingRepository
import com.example.daggerhiltwithgpt.data.room.MessagingRoomDataBase
import com.example.daggerhiltwithgpt.data.room.RoomMessage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomDbModule {


    @Provides
    @Singleton
    fun provideRoomDatabase(app: Application) =
         Room.databaseBuilder<MessagingRoomDataBase>(
            app.applicationContext,
            MessagingRoomDataBase::class.java,
            "GPTChat_DataBase"
        ).fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun provideDao(db: MessagingRoomDataBase) = db.messagingDao()

    @Provides
    @Singleton
    fun provideMessagingRepository(api: MessagingDAO): MessagingRepo {
        return MessagingRepository(api)
    }

//    @Provides
//    fun provideEntity() = RoomMessage()



}