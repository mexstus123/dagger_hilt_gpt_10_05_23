package com.example.daggerhiltwithgpt.ui.screens

import android.speech.tts.TextToSpeech
import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.daggerhiltwithgpt.data.MainViewModel
import com.example.daggerhiltwithgpt.ui.components.MessageBubble
import com.example.daggerhiltwithgpt.ui.components.MessagingScaffold
import java.util.*


@Composable
fun MessagingScreen(navController: NavHostController) {

    val ctx = LocalContext.current

    val mainViewModel = hiltViewModel<MainViewModel>()

    val messageList = mainViewModel.messageList.observeAsState(listOf())

    val isLoading by mainViewModel.isLoading.collectAsState()

    LaunchedEffect(key1 = isLoading) {
        Log.d("ERROR", "speech error tteeee")
        if (mainViewModel.replyReceived.value) {
            mainViewModel.resetReplyReceived()
            mainViewModel.tts = TextToSpeech(
                ctx,
                TextToSpeech.OnInitListener {
                    if (it == TextToSpeech.SUCCESS) {
                        mainViewModel.tts.language = Locale.ENGLISH
                        mainViewModel.tts.setSpeechRate(1.0F)
                        mainViewModel.tts.speak(
                            messageList.value.first().messageContent,
                            TextToSpeech.QUEUE_ADD,
                            null
                        )
                    } else {
                        Log.d("ERROR", "speech error")
                    }
                }
            )
        }
    }



    LaunchedEffect(key1 = Unit){
        mainViewModel.initializeMessages()
    }

    MessagingScaffold(
        navController = navController,
        receiverName = "testingName",
    ) {
        Surface(
            modifier = Modifier
                .padding(it)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(),
                reverseLayout = true
            ) {
//                if (isLoading){
//                    item {
//                        CircularProgressIndicator()
//                    }
//                }

                items(messageList.value.size) { index ->

                    Spacer(modifier = Modifier.height(10.dp))
                    MessageBubble(
                        onClick = {

                        },
                        messageContent = messageList.value[index].messageContent,
                        messageSenderIsUser = messageList.value[index].senderIsUser,
                        hasError = messageList.value[index].wasError
                    )
                    Spacer(modifier = Modifier.height(10.dp))

                }
            }
        }
    }
}