package com.example.daggerhiltwithgpt.ui.components

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.speech.RecognizerIntent
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mic
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.*

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.daggerhiltwithgpt.R
import com.example.daggerhiltwithgpt.data.MainViewModel


@Composable
fun MessagingTypingBar(
    typingWord: MutableState<String>,
    sendMessageOnClick: () -> Unit,
    modifier: Modifier
) {
    val mainViewModel = hiltViewModel<MainViewModel>()

    val ctx = LocalContext.current as Activity

    val isLoading by mainViewModel.isLoading.collectAsState()

    val microphoneActivityLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartActivityForResult(),
        onResult = { result ->
            Log.d("microphone","${result.data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)}")
            if (result.data != null && result.resultCode == RESULT_OK){
                val resultArray = result.data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                if (!resultArray.isNullOrEmpty()){
                    Log.d("microphone","$resultArray")
                    typingWord.value = resultArray[0].toString()
                }

            }

        }
    )

    Row(
        modifier = modifier
            .fillMaxHeight()
            .wrapContentHeight(unbounded = true)
    ) {

        OutlinedTextField(
            textStyle = LocalTextStyle.current.copy(fontSize = 14.sp),
            value = typingWord.value,
            onValueChange = { typingWord.value = it },
            modifier = modifier
                .padding(20.dp, 15.dp, 10.dp, 15.dp)
                .weight(0.85F),
            shape = RoundedCornerShape(22.dp),

            keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
            maxLines = 5,
            trailingIcon = {
                IconButton(modifier = Modifier
                    .padding(0.dp, 0.dp, 4.dp, 0.dp),
                    onClick = {
                        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
                        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Speak")
                        microphoneActivityLauncher.launch(intent)
                }) {
                    Icon(
                        imageVector = Icons.Filled.Mic,
                        //tint = MaterialTheme.colorScheme.onBackground,
                        contentDescription = stringResource(R.string.mic_icon),
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(5.dp)
                    )
                }
            }
        )

        Row(
            modifier = Modifier
                .weight(0.15F)
                .fillMaxWidth()
                .align(Alignment.CenterVertically)
                .height(65.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {

            if (isLoading){
                CircularProgressIndicator(
                    modifier = Modifier
                        .fillMaxSize(0.75F)
                        ,
                )
            }else {
            Surface(
                Modifier.fillMaxSize(0.75F),
                shape = RoundedCornerShape(120.dp),
                color = MaterialTheme.colorScheme.primary

            ) {

                    IconButton(onClick = sendMessageOnClick) {
                        Icon(
                            imageVector = Icons.Filled.Send,
                            //tint = MaterialTheme.colorScheme.onBackground,
                            contentDescription = stringResource(R.string.send_icon),
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(12.dp, 2.dp, 8.dp, 2.dp),
                            tint = MaterialTheme.colorScheme.background
                        )
                    }
                }
            }
        }
    }
}