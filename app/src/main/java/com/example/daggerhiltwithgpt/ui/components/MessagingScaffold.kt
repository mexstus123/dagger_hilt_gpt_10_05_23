package com.example.daggerhiltwithgpt.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController



@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MessagingScaffold(
    navController: NavHostController,
    receiverName: String,
    pageContent: @Composable (innerPadding: PaddingValues) -> Unit = {},
    ) {

    Scaffold(
        topBar = {
            MessagingTopAppBar(
                name = receiverName,
                onClickBack = {
                    //can put navigation here easily
                },
                OptionsButtonOnClick = {

                }
            )
        },

        bottomBar = {
            Box(
                modifier = Modifier
                    .wrapContentHeight(unbounded = true)
                    .fillMaxWidth()
            ) {
                MessagingBottomBar(
                    Modifier.align(Alignment.BottomCenter),
                    )
            }

              },
        content = { innerPadding -> pageContent(innerPadding) },
        modifier = Modifier.wrapContentHeight()
    )
}
