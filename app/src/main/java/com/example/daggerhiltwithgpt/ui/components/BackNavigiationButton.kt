package com.example.daggerhiltwithgpt.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.daggerhiltwithgpt.R


/**
 * Composable function that displays a back navigation button with an arrow pointing left, allowing for user navigation.
 * @param onClick A lambda that gets called when the button is clicked.
 * @param size The size of the button. Default value is 63.
 * @param modifier An instance of [Modifier] that is used to apply various layout modifications to the button.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BackNavigationButton(
    onClick: () -> Unit = {},
    size: Int = 60,
    modifier: Modifier
){

    IconButton(
        modifier = modifier
            .size(size.dp)
            .fillMaxHeight()
            .padding(),
        onClick = onClick,
    ) {
        Icon(
            imageVector = Icons.Filled.KeyboardArrowLeft,
            contentDescription = stringResource(R.string.back_arrow),
            // specify the tint color for the icon
            tint = MaterialTheme.colorScheme.primary,
            modifier = Modifier.fillMaxSize().padding()
        )
    }
}