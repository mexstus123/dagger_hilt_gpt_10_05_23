package com.example.daggerhiltwithgpt.ui.components

import androidx.compose.foundation.layout.*

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.material3.CardDefaults.cardColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.daggerhiltwithgpt.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MessageBubble(
    onClick: () -> Unit = {},
    messageContent: String = "",
    messageSenderIsUser: Boolean = true,
    hasError: Boolean = false
) {


    Column(Modifier.wrapContentSize()
        .fillMaxWidth()
        .padding(5.dp)) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = when (messageSenderIsUser) {
                true -> {
                    Arrangement.End
                }
                false -> {
                    Arrangement.Start
                }
            },
            verticalAlignment = Alignment.CenterVertically
        ) {
            if (!messageSenderIsUser) {
                Spacer(modifier = Modifier.weight(0.15F, false))
            }

            Card(
                modifier = Modifier.weight(0.85F, false),
                //setting tail shape here
                shape = when (messageSenderIsUser) {
                    true -> {
                        RoundedCornerShape(20.dp, 20.dp, 0.dp, 20.dp)
                    }
                    false -> {
                        RoundedCornerShape(20.dp, 20.dp, 20.dp, 0.dp)
                    }
                },
                onClick = {

                },
                colors = when (messageSenderIsUser) {
                    true -> {
                        cardColors(
                            MaterialTheme.colorScheme.primary,
                            MaterialTheme.colorScheme.onPrimary
                        )
                    }
                    false -> {
                        cardColors(
                            MaterialTheme.colorScheme.secondaryContainer,
                            MaterialTheme.colorScheme.onSecondaryContainer
                        )
                    }
                }

            ) {

                Text(
                    modifier = Modifier.padding(10.dp),
                    text = messageContent
                )

            }
            if (messageSenderIsUser) {
                if (hasError){
                    Icon(
                        imageVector = Icons.Outlined.Info,
                        //tint = MaterialTheme.colorScheme.onBackground,
                        contentDescription = stringResource(R.string.options_icon),
                        modifier = Modifier
                            .fillMaxSize()
                            .weight(0.15F, false)
                        ,
                        tint = MaterialTheme.colorScheme.error
                    )
                }else{
                    Spacer(modifier = Modifier.weight(0.15F, false))
                }

            }
        }
    }
}

