package com.example.daggerhiltwithgpt.ui.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.daggerhiltwithgpt.data.MainViewModel
import com.example.daggerhiltwithgpt.ui.navigation.Screen
import com.example.daggerhiltwithgpt.ui.screens.MessagingScreen


@Composable
fun BuildNavigationGraph(){

    val navController = rememberNavController()


    NavHost(
        navController = navController,
        startDestination = Screen.Messaging.route
    ){
//        composable(Screen.Categories.route){ CategoriesScreen(navController,mainViewModel) }
        composable(Screen.Messaging.route){ MessagingScreen(navController) }

    }
}