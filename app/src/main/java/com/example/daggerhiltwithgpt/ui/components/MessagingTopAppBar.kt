package com.example.daggerhiltwithgpt.ui.components


import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.daggerhiltwithgpt.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MessagingTopAppBar(
    onClickBack: () -> Unit = {},
    name : String,
    OptionsButtonOnClick: () -> Unit = {}

){
    // the CenterAlignedTopAppBar composable displays the top app bar
    CenterAlignedTopAppBar(
        modifier = Modifier.shadow(8.dp),
        // the title of the top app bar
        title = {
                // display the app title text
                Text(
                    text = name,
                    fontWeight = FontWeight.SemiBold,
                )
        },
        // the navigation icon for the top app bar
        navigationIcon = {
            // the IconButton displays the navigation icon and calls the onClick lambda when clicked
            BackNavigationButton(onClickBack, modifier = Modifier)
        },
        actions = {
            IconButton(onClick = OptionsButtonOnClick ) {
                Icon(
                    imageVector = Icons.Default.MoreVert,
                    //tint = MaterialTheme.colorScheme.onBackground,
                    contentDescription = stringResource(R.string.options_icon),
                    modifier = Modifier
                        .fillMaxSize()
                        .padding()
                    ,
                    tint = MaterialTheme.colorScheme.outline
                )
            }
        }
    )
}
