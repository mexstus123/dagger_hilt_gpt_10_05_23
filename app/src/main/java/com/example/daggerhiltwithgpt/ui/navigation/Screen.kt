package com.example.daggerhiltwithgpt.ui.navigation

/**
 * A sealed class representing the screens in the app.
 *
 * @property route a string representing the route for the screen
 */
sealed class Screen(
    val route: String
){
    /**
     * Each one of these objects represents a screen in the application.
     */
    object Categories : Screen("Categories")
    object Messaging : Screen("Messaging")


}
