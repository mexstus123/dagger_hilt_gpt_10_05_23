package com.example.daggerhiltwithgpt.ui.components

import androidx.compose.foundation.layout.*

import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.daggerhiltwithgpt.data.ChatMessage
import com.example.daggerhiltwithgpt.data.MainViewModel

@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterial3Api::class)
@Composable
fun MessagingBottomBar(
    modifier: Modifier,
) {
    val viewModel = hiltViewModel<MainViewModel>()

    val firstTextValue = rememberSaveable { mutableStateOf("") }

    Surface(
        modifier = modifier
            .wrapContentHeight()
            .shadow(50.dp),
        color = MaterialTheme.colorScheme.background,
    ) {
        MessagingTypingBar(
            typingWord = firstTextValue,
            sendMessageOnClick = {
                if (firstTextValue.value != "") {
                    viewModel.sendMessage(
                        ChatMessage(
                            content = firstTextValue.value
                        )
                    )
                    firstTextValue.value = ""
                }
            }
            ,
            modifier = Modifier
                .wrapContentHeight()
        )
    }
}


